# Reviews test task by *Anton Filatov*

## focus
In a time-constraint evironment the plan was to:

- implement networking layer first
- then have a basic UI with a draft of single-resonsibilty UI architecture
- then when API is stabilized implement tests for networking and service layer
- after all make UI more eye-candy
- implement persint layer

Unfourtinaly only first two steps were more or less accomplished.
Persistency could be relatively easy acheived with `URLSessionCache` yet thats a bit hacky way.

Like in a real life there is no perfect code, so there are few `TODO:` comments.
Also error handling and responce parsing is not perfect and could use some refactoring to make it more generic.

happy reviewing!