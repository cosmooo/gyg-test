//
//  Review.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

/// Represents Review entity
public struct Review {
    let reviewId: Int
    let title: String?
    let text: String

}
