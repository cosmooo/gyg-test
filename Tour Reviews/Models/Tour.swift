//
//  Tour.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

/// Represents Tour entity
public struct Tour {
    let status: Bool
    let totalComments: Int
    let reviews: [Review]
}
