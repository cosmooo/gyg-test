//
//  APIRequest.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

/// Convenient generic protocol for creating APIRequests
public protocol APIRequest {
    var baseURL: URL { get }
    var endpoint: Endpoint { get }
    var query: [String: String] { get }
    var headers: [HTTPHeader] { get }
    var body: Data? { get }
}

extension APIRequest {
    func urlRequest() -> URLRequest {
        var url = baseURL.appendingPathComponent(endpoint.path)

        var components = URLComponents(string: url.absoluteString)
        components?.queryItems = query.map(URLQueryItem.init)

        if let queryURL = components?.url {
            url = queryURL
        }

        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.rawValue

        headers.forEach { (header) in
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }

        request.httpBody = body
        return request
    }
}
