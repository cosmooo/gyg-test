//
//  APIResponse.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

public protocol APIResponse {
    var httpResponse: HTTPURLResponse? { get }
    var contentType: MIMEType? { get }
    var data: NSData? { get }
    var error: Error? { get }
}
