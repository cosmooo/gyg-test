//
//  Endpoint.swift
//  Tour Reviews
//
//  Created by Anton on 25/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

/// Defines generic-purpose endpoint
public protocol Endpoint {

    /// Path of the endpoint, e.g. "/users"
    var path: String { get }

    /// HTTP Method
    var method: HTTPMethod { get }

    /// Default headers requered by the endpoint, key-value pairs
    var requeredHeaders: [HTTPHeader] { get }
}

/// Strongly typed HTTP Methods used by backend
public enum HTTPMethod: String {
    case GET    = "GET"
    case POST   = "POST"
    case PUT    = "PUT"
    case DELETE = "DELETE"
}

public typealias MIMEType = String

/// Represents key-value pairs of most commonly used http headers
public enum HTTPHeader {
    case accept(MIMEType)
    case contentType(MIMEType)
    case custom(String, String)

    var key: String {
        switch self {
        case .accept:
            return "Accept"
        case .contentType:
            return "Content-Type"
        case .custom(let key, _):
            return key
        }
    }

    var value: String {
        switch self {
        case .accept(let type):
            return type
        case .contentType(let type):
            return type
        case .custom(_, let value):
            return value
        }
    }
}
