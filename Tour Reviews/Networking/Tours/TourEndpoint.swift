//
//  TourEndpoint.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

/// Endpoint used to get information about the tour and and sumbit the review

/// `Assuming` that the backend structure is: `baseURL/city/event/ThisEnum`
enum TourEndpoint: Endpoint {
    /// Returns reviews for particular tour
    case reviews

    /// Allows to submit the review for the tour
    case submitReview
}

extension TourEndpoint {
    var path: String {
        switch self {
        case .reviews:
            return "reviews.json"
        case .submitReview:
            return "submitReview"
        }
    }
    var method: HTTPMethod {
        switch self {
        case .reviews:
            return .GET
        case .submitReview:
            return .POST
        }
    }

    var requeredHeaders: [HTTPHeader] {
        switch self {
        case .reviews:
            return [.accept("application/json")]
        case .submitReview:
            return [.contentType("application/json")]
        }
    }
}
