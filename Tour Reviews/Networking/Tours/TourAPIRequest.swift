//
//  TourAPIRequest.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

/// Tour-specific API request
struct TourAPIRequest: APIRequest {
    var baseURL: URL

    var endpoint: Endpoint

    var query: [String : String]

    var headers: [HTTPHeader]

    var body: Data?

    /// String representation of city and tour for the requrst.
    //TODO: Please find a better way to address it
    typealias City = String
    typealias Tour = String

    init(baseURL: URL, city: City, tour: Tour, endpoint: TourEndpoint, query: [String : String], headers: [HTTPHeader], body: Data?) {
        self.baseURL = baseURL
        self.baseURL.appendPathComponent(city)
        self.baseURL.appendPathComponent(tour)
        self.endpoint = endpoint
        self.query = query
        self.headers = headers
        self.headers.append(contentsOf: endpoint.requeredHeaders)
        self.body = body
    }
}
