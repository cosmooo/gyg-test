//
//  TourReviewResponseModel.swift
//  Tour Reviews
//
//  Created by Anton on 27/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

struct TourReviewResponseModel: Decodable {
    let status: Bool
    let total_reviews_comments: Int
    let data: [ReviewItemModel]
}

struct ReviewItemModel: Decodable {
    let review_id: Int
    let rating: String
    let title: String?
    let message: String
    let author: String
    let foreignLanguage: Bool
    let date: String
    let date_unformatted: Dictionary<String,String>
    let languageCode: String
    let traveler_type: String?
    let reviewerName: String
    let reviewerCountry: String
}
