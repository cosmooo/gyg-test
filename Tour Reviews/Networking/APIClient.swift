//
//  APIClient.swift
//  Tour Reviews
//
//  Created by Anton on 25/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

/// Generic API client. Executes APIRequests
public class APIClient {

    /// Generic shared session. Feel free to override or use mocked for tests
    public var session = URLSession.shared

    /// Generic method to execute APIRequrst
    public func perform(_ request: APIRequest, completion: @escaping (Data?, Error?) -> Void) {
        let request = request.urlRequest()
        let task = session.dataTask(with: request) { (data, response, error) in

            guard let httpResponse = response as? HTTPURLResponse else {
                fatalError()
            }

            guard error == nil else {
                completion(nil, error)
                return
            }

            if 200 ... 299 ~= httpResponse.statusCode {
                completion(data, error)
            } else {
                let error = NSError(domain: "NetworkDomain",
                                    code: httpResponse.statusCode,
                                    userInfo: [NSLocalizedDescriptionKey:"Connection error"])
                completion(nil, error)
            }
        }
        task.resume()
    }
}

