//
//  ReviewsViewController.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import UIKit

let reviewTableViewCellID = "reuseIdentifier"

/// Protocol that describes how to present / reload conent on reviews screen
protocol ReviewViewable {
    func reload()
    func showError(_ title: String, buttonTitle: String, error: String)
}

class ReviewsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    let presenter = ReviewsViewPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = presenter
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reviewTableViewCellID)
        presenter.viewable = self
        setupAppearance()
    }

    func setupAppearance() {
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.willAppear()
        
    }
}

extension ReviewsViewController: ReviewViewable {
    func reload() {
        tableView.reloadData()
    }

    func showError(_ title: String, buttonTitle: String, error: String) {
        let alert = UIAlertController(title: title, message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: buttonTitle, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
