//
//  ReviewsViewPresenter.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import UIKit

/// Implements reviews screen logic, to make it separate from UI layer and not blow up viewController
class ReviewsViewPresenter: NSObject, UITableViewDataSource {

    private var reviews: [Review] = []
    var viewable: ReviewViewable?

    // MARK: livecycle

    func willAppear() {
        TourService().getTempelhofReviews {[weak self] (reviews, error) in
            if let error = error {
                let errorTitle = NSLocalizedString("An error occurred", comment: "Error alert title on reviews screen")
                let buttonTitle = NSLocalizedString("Ok", comment: "Ok alert action on reviews screen")
                self?.viewable?.showError(errorTitle, buttonTitle: buttonTitle, error: error.localizedDescription)
            } else {
                self?.reviews = reviews
            }
            self?.viewable?.reload()
        }
    }

    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reviewTableViewCellID, for: indexPath)
        let review = reviews[indexPath.row]
        cell.textLabel?.text = review.title
        cell.detailTextLabel?.text = review.text
        return cell
    }
}
