//
//  TourService.swift
//  Tour Reviews
//
//  Created by Anton on 26/07/2018.
//  Copyright © 2018 GetYourGuide. All rights reserved.
//

import Foundation

public typealias ReviewsCallback = ([Review], Error?) -> Void

/// Fetches and persists tours
public class TourService {

    /// Convenience method for testing task
    public func getTempelhofReviews(callback: @escaping ReviewsCallback) {
        let tour = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776"
        let city = "berlin-l17"
        getReviews(for: tour, in: city, callback: callback)
    }

    /// Fetches reviews from network
    public func getReviews(for tour: String, in city: String, count: Int = 5, page: Int = 0, callback: @escaping ReviewsCallback) {

        //TODO: it should be an injected property of the APIClient()
        let baseURL = URL(string: "https://www.getyourguide.com")!

        let query = ["count": "\(count)", "page" : "\(page)"]

        let request = TourAPIRequest(baseURL: baseURL,
                                     city: city,
                                     tour: tour,
                                     endpoint: .reviews,
                                     query: query,
                                     headers: [],
                                     body: nil)
        APIClient().perform(request) { (data, error) in
            DispatchQueue.main.async {
                if let error = error {
                    callback([], error)
                } else if let data = data {
                    do {
                        let decoder = JSONDecoder()
                        let response = try decoder.decode(TourReviewResponseModel.self, from: data)

                        let reviews = response.data.map(self.mapReviews)
                        callback(reviews, nil)
                    } catch {
                        callback([], error)
                    }
                }
            }
        }
    }

    private func mapReviews(responseModel: ReviewItemModel) -> Review {
        return Review(reviewId: responseModel.review_id, title: responseModel.title, text:  responseModel.message)
    }
}
